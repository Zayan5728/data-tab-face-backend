const express=require('express');
const router=express.Router();
const Appctrl=require('../controllers/Appctrl')
router.get('/',Appctrl.home)
router.get('/apps',Appctrl.get)
router.get('/apps/analytics',Appctrl.getanalytics)
router.get('/apps/analytics/charts',Appctrl.getcharts)
router.get('/apps/details',Appctrl.getdetails)
router.get('/apps/formdata',Appctrl.getformdata)
router.get('/apps/Tabledata',Appctrl.getTabledata)
router.post('/',Appctrl.post)
router.put('/:id',Appctrl.put)
router.delete('/:id',Appctrl.remove)
router.patch('/:id',Appctrl.patch)
module.exports=router