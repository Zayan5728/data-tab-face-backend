const express=require('express')
const app=express();
const port=3000;
const bodyparser=require('body-parser')
const cors=require('cors')

const Approutes=require('./routes/Approutes')
app.use(express.json())
app.use(cors())
app.use((req,res,next)=>{
    res.set('Access-Control-Allow-Origin',"*")
   res.header(
        "Access-Control-Allow-Headers",
        "X-Requested-With"
        )
        next();
})
app.use('/',Approutes)
app.listen(port,()=>{
    console.log('server is runing on'+port)
}) 