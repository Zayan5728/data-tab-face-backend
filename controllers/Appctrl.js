const Apps = [
    { id: 1, name: 'app1', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 2, name: 'app2', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 3, name: 'app3', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 4, name: 'app4', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 5, name: 'app5', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 6, name: 'app6', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 7, name: 'app7', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 8, name: 'app8', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 9, name: 'app9', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 10, name: 'app10', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 11, name: 'app11', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 12, name: 'app12', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 13, name: 'app13', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 14, name: 'app14', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 15, name: 'app15', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 16, name: 'app16', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 17, name: 'app17', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 18, name: 'app18', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 19, name: 'app19', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 20, name: 'app20', models: ['model1', 'model2', 'model3', 'model4'] },
    { id: 21, name: 'app21', models: ['model1', 'model2', 'model3', 'model4'] },



]
const analytics = require('../Data/Analyticsdata')
const charttypes = require('../Data/charttypes')
const appdetails = require('../Data/appdetails')
const formFields=require('../Data/dynamicform')
const Tabledata=require('../Data/Tabledata')

const home = (req, res) => {
    res.status(200);
    res.send('Apps and models')
}
const get = (req, res) => {
    const page = parseInt(req.query.page) || 1;
    const limit = parseInt(req.query.limit) || 10;
    const search = req.query.search || '';
    const dir = req.query.dir || 'asc'
    console.log(`${search}`)
    const filteredData = Apps.filter(app =>
        app.name.includes(search)
    );

    const totalCount = filteredData.length;

    const totalPages = Math.ceil(totalCount / limit);

    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;

    const result = filteredData.slice(startIndex, endIndex);

    const response = {
        metadata: {
            totalCount: totalCount,
            totalPages: totalPages
        },
        data: result
    };

    res.status(200).json(response);
};

const getanalytics = (req, res) => {
    const id=req.query.id || 0
    res.json(analytics).status(200)
}

const getcharts = (req, res) => {
    const id = req.query.id || '';
    // console.log(id);
    
    for (let i = 0; i < charttypes.length; i++) {
        // console.log(charttypes[i]);
        if (charttypes[i].id === id) {
            res.status(200).send(charttypes[i]);
            return;
        }
    }
    res.status(200).json(charttypes)};


const getdetails = (req, res) => {
    res.status(200)
    res.send(appdetails)
}
const getformdata=(req,res)=>{
    const id=req.query.id||''
    res.status(200)
    res.json(formFields)
}
const getTabledata=(req,res)=>{
    const id=req.query.id||''
    res.status(200)
    res.json(Tabledata)
}
const post = (req, res) => {
    const data = req.body
    films.push(data)
    res.send('created')
    res.status(201)
}
// // get by id method is used for the details.
// const getbyid=(req,res)=>{
//     const id=+req.params.id;
//     for(let i=0;i<films.length;i++){
//         if(films[i].id===id)
//         res.status(200)
//         return;
//     }
//     res.status(404)
//     res.send('not found')
// }
const remove = (req, res) => {
    const id = +req.params.id
    for (let i = 0; i < films.length; i++) {
        if (films[i].id === id) {
            films.splice(i, 1)
        }
    }
    res.status(204)
    res.send('')
}

const put = (req, res) => {
    const id = +req.params.id
    const data = req.body
    for (let i = 0; i < films.length; i++) {
        if (films[i].id === id) {
            films[i].name = data.name
            films[i].number = data.number
            res.status(204)
            res.send('')
            return;


        }

    }
    res.status(404)
    res.send('not found')
}
const patch = (req, res) => {
    const id = +req.params.id
    const data = req.body
    for (let i = 0; i < films.length; i++) {
        if (films[i].id === id) {
            for (var key in data) {
                films[i][key] = data[key]
                res.status(200)
                res.send('')
                return;

            }
        }
    }
    res.status(404).send

}
// put is fully update one,where as patch is partial one.
// we never update an id.
module.exports = {
    home, get, post, remove, put, patch, getanalytics, getcharts, getdetails,getformdata,getTabledata
}
//  crud operations create read update(partially(patch),put(fully)) delete
// put,post,patch has the body user sending request as a body.

































































































































