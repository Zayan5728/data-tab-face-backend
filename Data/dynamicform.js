const formFields = [
    {
        label: "Location",
        input_type: "text",
        name: "location"
    },
    {
        label: "Location Type",
        input_type: "select",
        name: "location_type",
        options: ["choose a location", "City", "Suburb", "Rural Area"]
    },
    {
        label: "Latitude",
        input_type: "number",
        name: "latitude"
    },
    {
        label: "Longitude",
        input_type: "number",
        name: "longitude"
    }
];

module.exports={
    formFields
}