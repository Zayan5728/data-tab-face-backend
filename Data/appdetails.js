const appdetails={  
        models: [
            {
                id: 1,
                name: "Location"
            },
            {
                id: 2,
                name: "Location Type"
            },
            {
                id: 3,
                name: "Polygon"
            },
            {
                id: 4,
                name: "Marker"
            },
            {
                id: 5,
                name: "Layer"
            },
            {
                id: 6,
                name: "Polygon marker map"
            },
            {
                id: 7,
                name: "Location type map"
            },
            {
                id: 8,
                name: "Location Polygon map"
            },
            {
                id: 9,
                name: "satellite"
            },
            {
                id: 10,
                name: "Norad ID"
            },   
            {
                id: 11,
                name: "Satellite Norad id map"
            }
        ]
    }
    module.exports={
        appdetails
    }
  
    
